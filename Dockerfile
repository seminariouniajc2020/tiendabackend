# https://spring.io/guides/gs/spring-boot-docker/
#FROM openjdk:11
FROM adoptopenjdk/openjdk11:latest
VOLUME /tmp

ADD /target/SegundoCorte-0.0.1-SNAPSHOT.jar dockerdemo.jar
ENTRYPOINT ["java", "-jar", "dockerdemo.jar"]