package com.seminario.api

data class Greeting(val id: Long, val content: String)