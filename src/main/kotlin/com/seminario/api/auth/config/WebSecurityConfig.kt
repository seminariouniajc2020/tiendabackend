package com.seminario.api.auth.config

import com.seminario.api.auth.filters.JwtRequestFilter
import com.seminario.api.utils.Constants
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpMethod
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter

@Configuration
@EnableWebSecurity
class WebSecurityConfig: WebSecurityConfigurerAdapter()  {

    @Autowired
    val userDetailsService: UserDetailsService? = null

    @Autowired
    val jwtRequestFilter: JwtRequestFilter? = null

    @Throws(Exception::class)
    override fun configure(auth: AuthenticationManagerBuilder) {
        auth.userDetailsService(userDetailsService).passwordEncoder(getPasswordEncoder())
    }

    @Throws(Exception::class)
    override fun configure(http: HttpSecurity) {
        http.csrf().disable().authorizeRequests()
                .mvcMatchers(HttpMethod.POST,"${Constants.URL_BASE_AUTH}/*").permitAll()
                .mvcMatchers(HttpMethod.GET, Constants.URL_BASE_PERMISSION).hasAuthority("read:permissions")
                .mvcMatchers(HttpMethod.GET,"${Constants.URL_BASE_PERMISSION}/*").hasAuthority("read:permissions")
                .mvcMatchers(HttpMethod.POST, Constants.URL_BASE_PERMISSION).hasAuthority("create:permissions")
                .mvcMatchers(HttpMethod.PUT,"${Constants.URL_BASE_PERMISSION}/*").hasAuthority("update:permissions")
                .mvcMatchers(HttpMethod.DELETE,"${Constants.URL_BASE_PERMISSION}/*").hasAuthority("delete:permissions")
                .mvcMatchers(HttpMethod.GET, Constants.URL_BASE_ROLES).hasAuthority("read:roles")
                .mvcMatchers(HttpMethod.GET,"${Constants.URL_BASE_ROLES}/*").hasAuthority("read:roles")
                .mvcMatchers(HttpMethod.POST, Constants.URL_BASE_ROLES).hasAuthority("create:roles")
                .mvcMatchers(HttpMethod.PUT,"${Constants.URL_BASE_ROLES}/*").hasAuthority("update:roles")
                .mvcMatchers(HttpMethod.POST,"${Constants.URL_BASE_ROLES}/*/permissions").hasAuthority("update:roles")
                .mvcMatchers(HttpMethod.DELETE, "${Constants.URL_BASE_ROLES}/*").hasAuthority("delete:roles")
                .mvcMatchers(HttpMethod.GET, Constants.URL_BASE_USERS).hasAuthority("read:users")
                .mvcMatchers(HttpMethod.GET, "${Constants.URL_BASE_USERS}/*").hasAuthority("read:users")
                .mvcMatchers(HttpMethod.POST, Constants.URL_BASE_USERS).hasAuthority("create:users")
                .mvcMatchers(HttpMethod.PUT, "${Constants.URL_BASE_USERS}/*").hasAuthority("update:users")
                .mvcMatchers(HttpMethod.POST, "${Constants.URL_BASE_USERS}/*/roles").hasAuthority("update:users")
                .mvcMatchers(HttpMethod.DELETE, "${Constants.URL_BASE_USERS}/*").hasAuthority("delete:users")
                .mvcMatchers(HttpMethod.GET, Constants.URL_BASE_PRODUCTO).hasAuthority("read:products")
                .mvcMatchers(HttpMethod.GET, "${Constants.URL_BASE_PRODUCTO}/*").hasAuthority("read:products")
                .mvcMatchers(HttpMethod.GET, Constants.URL_BASE_COMPRAS).hasAuthority("read:purchases")
                .mvcMatchers(HttpMethod.POST, Constants.URL_BASE_COMPRAS).hasAuthority("create:purchases")
                .mvcMatchers(HttpMethod.GET, "${Constants.URL_BASE_COMPRAS}/*").hasAuthority("read:purchases")
                .and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and().addFilterBefore(jwtRequestFilter!!, UsernamePasswordAuthenticationFilter::class.java)
    }

    @Bean
    fun getPasswordEncoder(): PasswordEncoder {
        return BCryptPasswordEncoder()
    }

    @Bean
    override fun authenticationManager(): AuthenticationManager {
        return super.authenticationManager()
    }
}