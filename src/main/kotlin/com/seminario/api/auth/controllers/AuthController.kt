package com.seminario.api.auth.controllers

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import com.seminario.api.auth.utils.JwtUtil
import com.seminario.api.auth.dto.UserDTO
import com.seminario.api.auth.services.MyUserDetailsService
import com.seminario.api.auth.models.AuthRequest
import com.seminario.api.auth.models.AuthResponse
import com.seminario.api.auth.models.MyUserDetails
import com.seminario.api.auth.models.User
import com.seminario.api.exceptions.BadCredentiaslException
import com.seminario.api.utils.Constants
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@RestController
@RequestMapping(Constants.URL_BASE_AUTH)
class AuthController {

    @Autowired
    private val authManager: AuthenticationManager? = null

    @Autowired
    private val userDetailsService: MyUserDetailsService? = null

    @Autowired
    private val jwtTokenUtil: JwtUtil? = null

    /**
     * Display the token access, prefix and scopes.
     * @param authRequest: AuthRequest
     * @return ResponseEntity<AuthResponse>
     */
    @PostMapping("/token")
    fun authenticate( @RequestBody authRequest: AuthRequest): ResponseEntity<AuthResponse> {
        try {
            authManager!!.authenticate(
                    UsernamePasswordAuthenticationToken(authRequest.email, authRequest.password)
            )
        } catch (e: Exception) {
            throw BadCredentiaslException(Constants.MESSAGE_BAD_CREDENTIALS)
        }
        val userDetails: MyUserDetails = userDetailsService!!.loadUserByUsername(authRequest.email)
        val accessToken: String? = jwtTokenUtil!!.generateToken(userDetails)
        return ResponseEntity(
                AuthResponse(
                        accessToken = accessToken
                ),
                HttpStatus.OK
        )
    }

    /**
     * Create a new user.
     * @param user: UserDTO
     * @return ResponseEntity
     */
    @PostMapping("/signup")
    fun signup( @RequestBody user: UserDTO): ResponseEntity<Any> {
        val initials: String = user.name!!.substring(0,1) + user.lastName!!.substring(0,1)
        user.picture = "https://i1.wp.com/cdn.auth0.com/avatars/$initials.png?ssl=1"
        userDetailsService!!.save(User(user))
        return ResponseEntity(HttpStatus.CREATED)
    }

    /**
     * Get a user information.
     * @param jwt: String
     * @return ResponseEntity<UserDTO>
     */
    @GetMapping("/userinfo")
    fun userinfo( @RequestHeader("Authorization") jwt: String ): ResponseEntity<UserDTO> {
        val user: User = userDetailsService!!.userinfo(jwt)
        return ResponseEntity(UserDTO(user, true), HttpStatus.OK)
    }

    /**
     * Get a user information.
     * @param jwt: String
     * @param info: UserDTO
     * @return ResponseEntity
     */
    @PutMapping("/userinfo")
    fun updateUserinfo(
            @RequestHeader("Authorization") jwt: String,
            @RequestBody info: UserDTO
    ): ResponseEntity<Any> {
        userDetailsService!!.updateUserinfo(jwt, User(info))
        return ResponseEntity(HttpStatus.OK)
    }
}