package com.seminario.api.auth.controllers

import com.seminario.api.auth.dto.CompraDTO
import com.seminario.api.auth.dto.ReqCompra
import com.seminario.api.auth.models.Compra
import com.seminario.api.auth.services.CompraService

import com.seminario.api.utils.Constants
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping(Constants.URL_BASE_COMPRAS)
class CompraController {

    @Autowired
    private val compraService: CompraService? = null

    /**
     * Display a list of purchases
     * @return ResponseEntity<List<CompraDTO>>
     */
    @GetMapping
    fun getAll(): ResponseEntity<List<CompraDTO>> {
        val compras: List<CompraDTO> = compraService!!.getAll().map {
            compra -> CompraDTO(compra)
        }
        return ResponseEntity(compras, HttpStatus.OK)
    }

    /**
     * Display a purchase
     * @param id: Long
     * @return \ResponseEntity<UserDTO>
     */
    @GetMapping("/{id}")
    fun getById(@PathVariable("id") id: Long): ResponseEntity<CompraDTO> {
        val compra: Compra = compraService!!.getById(id)
        return ResponseEntity(CompraDTO(compra), HttpStatus.OK)
    }

    /**
     * Create a new purchase
     * @param compra: ReqCompra
     * @return ResponseEntity
     */
    @PostMapping
    fun create(@RequestBody compra: ReqCompra): ResponseEntity<Any> {
        compraService!!.save(compra.idProducto!!, compra.cantidad!!, compra.idUser!!)
        return ResponseEntity(HttpStatus.CREATED)
    }
}