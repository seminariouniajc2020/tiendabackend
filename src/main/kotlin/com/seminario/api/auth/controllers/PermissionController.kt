package com.seminario.api.auth.controllers

import com.seminario.api.auth.dto.PermissionDTO
import com.seminario.api.auth.models.Permission
import com.seminario.api.auth.services.PermissionService
import com.seminario.api.utils.Constants
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping(Constants.URL_BASE_PERMISSION)
class PermissionController {

    @Autowired
    private val permissionService: PermissionService? = null

    /**
     * Display a list of permissions
     * @return ResponseEntity<List<PermissionDTO>>
     */
    @GetMapping
    fun getAll(): ResponseEntity<List<PermissionDTO>> {
        val permissions: List<PermissionDTO> = permissionService!!.getAll().map {
            permission -> PermissionDTO(permission, false)
        }
        return ResponseEntity(permissions, HttpStatus.OK)
    }

    /**
     * Display a permissions
     * @param id: Long
     * @return ResponseEntity<PermissionDTO>
     */
    @GetMapping("/{id}")
    fun getById(@PathVariable("id") id: Long): ResponseEntity<PermissionDTO> {
        val permission: Permission = permissionService!!.getById(id)
        val permissionDTO = PermissionDTO(permission, true)
        return ResponseEntity(permissionDTO, HttpStatus.OK)
    }

    /**
     * Create a new permission
     * @param permission: PermissionDTO
     * @return ResponseEntity
     */
    @PostMapping
    fun create(@RequestBody permission: PermissionDTO): ResponseEntity<Any> {
        val permissionEntity = Permission(permission)
        permissionService!!.save(permissionEntity)
        return ResponseEntity(HttpStatus.CREATED)
    }

    /**
     * Update a permission
     * @param id: Long
     * @param permission: PermissionDTO
     * @return ResponseEntity
     */
    @PutMapping("/{id}")
    fun update(@PathVariable("id") id: Long, @RequestBody permission: PermissionDTO): ResponseEntity<Any> {
        val permissionEntity = Permission(permission)
        permissionService!!.update(id, permissionEntity)
        return ResponseEntity(HttpStatus.OK)
    }

    /**
     * Delete a permission
     * @param id: Long
     * @return ResponseEntity
     */
    @DeleteMapping("/{id}")
    fun delete(@PathVariable("id") id: Long): ResponseEntity<Any> {
        permissionService!!.delete(id)
        return ResponseEntity(HttpStatus.OK)
    }
}