package com.seminario.api.auth.controllers

import com.seminario.api.auth.dto.ProductoDTO
import com.seminario.api.auth.models.Producto
import com.seminario.api.auth.services.ProductoService
import com.seminario.api.utils.Constants
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping(Constants.URL_BASE_PRODUCTO)
class ProductoController {

    @Autowired
    private val productoService: ProductoService? = null

    /**
     * Display a list of products
     * @return ResponseEntity<List<ProductoDTO>>
     */
    @GetMapping
    fun getAll(): ResponseEntity<List<ProductoDTO>> {
        val products: List<ProductoDTO> = productoService!!.getAll().map {
            product -> ProductoDTO(product)
        }
        return ResponseEntity(products, HttpStatus.OK)
    }

    /**
     * Display a product
     * @param id: Long
     * @return \ResponseEntity<ProductDTO>
     */
    @GetMapping("/{id}")
    fun getById(@PathVariable("id") id: Long): ResponseEntity<ProductoDTO> {
        val product: Producto = productoService!!.getById(id)
        return ResponseEntity(ProductoDTO(product), HttpStatus.OK)
    }
}