package com.seminario.api.auth.controllers

import com.seminario.api.auth.dto.PermissionDTO
import com.seminario.api.auth.dto.RoleDTO
import com.seminario.api.auth.models.Role
import com.seminario.api.auth.services.RoleService
import com.seminario.api.utils.Constants
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping(Constants.URL_BASE_ROLES)
class RoleController {

    @Autowired
    private val roleService: RoleService? = null

    /**
     * Display a list of roles
     * @return ResponseEntity<List<RoleDTO>>
     */
    @GetMapping
    fun getAll(): ResponseEntity<List<RoleDTO>> {
        val roles: List<RoleDTO> = roleService!!.getAll().map {
            role -> RoleDTO(role, false)
        }
        return ResponseEntity(roles, HttpStatus.OK)
    }

    /**
     * Display a role
     * @param id: Long
     * @return \ResponseEntity<RoleDTO>
     */
    @GetMapping("/{id}")
    fun getById(@PathVariable("id") id: Long): ResponseEntity<RoleDTO> {
        val role: Role = roleService!!.getById(id)
        return ResponseEntity(RoleDTO(role, true), HttpStatus.OK)
    }

    /**
     * Create a new role
     * @param role: RoleDTO
     * @return ResponseEntity
     */
    @PostMapping
    fun create(@RequestBody role: RoleDTO): ResponseEntity<Any> {
        roleService!!.save(Role(role))
        val responseHeader = HttpHeaders()
        responseHeader.set("location", Constants.URL_BASE_PERMISSION + "/" + role.id)
        return ResponseEntity(responseHeader, HttpStatus.CREATED)
    }

    /**
     * Update a role
     * @param id: Long
     * @param role: RoleDTO
     * @return ResponseEntity
     */
    @PutMapping("/{id}")
    fun update(@PathVariable("id") id: Long, @RequestBody role: RoleDTO): ResponseEntity<Any> {
        roleService!!.update(id, Role(role))
        return ResponseEntity(HttpStatus.OK)
    }

    /**
     * Delete a role
     * @param id: Long
     * @return ResponseEntity
     */
    @DeleteMapping("/{id}")
    fun delete(@PathVariable("id") id: Long): ResponseEntity<Any> {
        roleService!!.delete(id)
        return ResponseEntity(HttpStatus.OK)
    }

    /**
     * Assign permissions to a role
     * @param id: Long
     * @param permissions: List<PermissionDTO>
     * @return ResponseEntity
     */
    @PostMapping("/{id}/permissions")
    fun assignPermissions(
            @PathVariable("id") id: Long,
            @RequestBody permissions: List<PermissionDTO>
    ): ResponseEntity<Any> {
        val ids: MutableList<Long> = mutableListOf()
        for (permission in permissions) {
            ids.add(permission.id!!)
        }
        roleService!!.assignPermissions(id, ids)
        return ResponseEntity(HttpStatus.OK)
    }
}