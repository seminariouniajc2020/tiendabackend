package com.seminario.api.auth.controllers

import com.seminario.api.auth.dto.RoleDTO
import com.seminario.api.auth.dto.UserDTO
import com.seminario.api.auth.models.User
import com.seminario.api.auth.services.MyUserDetailsService
import com.seminario.api.utils.Constants
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping(Constants.URL_BASE_USERS)
class UserController {

    @Autowired
    private val userService: MyUserDetailsService? = null

    /**
     * Display a list of users
     * @return ResponseEntity<List<UserDTO>>
     */
    @GetMapping
    fun getAll(): ResponseEntity<List<UserDTO>> {
        val users: List<UserDTO> = userService!!.getAll().map {
            user -> UserDTO(user, false)
        }
        return ResponseEntity(users, HttpStatus.OK)
    }

    /**
     * Display a user
     * @param id: Long
     * @return \ResponseEntity<UserDTO>
     */
    @GetMapping("/{id}")
    fun getById(@PathVariable("id") id: Long): ResponseEntity<UserDTO> {
        val user: User = userService!!.getById(id)
        return ResponseEntity(UserDTO(user, true), HttpStatus.OK)
    }

    /**
     * Create a new user
     * @param user: UserDTO
     * @return ResponseEntity
     */
    @PostMapping
    fun create(@RequestBody user:UserDTO): ResponseEntity<Any> {
        val initials: String = user.name!!.substring(0,1) + user.lastName!!.substring(0,1)
        user.picture = "https://i1.wp.com/cdn.auth0.com/avatars/$initials.png?ssl=1"
        userService!!.save(User(user))
        return ResponseEntity(HttpStatus.CREATED)
    }

    /**
     * Update a user
     * @param id: Long
     * @param user: userDTO
     * @return ResponseEntity
     */
    @PutMapping("/{id}")
    fun update(@PathVariable("id") id: Long, @RequestBody user: UserDTO): ResponseEntity<Any> {
        userService!!.update(id, User(user))
        return ResponseEntity(HttpStatus.OK)
    }

    /**
     * Delete a user
     * @param id: Long
     * @return ResponseEntity
     */
    @DeleteMapping("/{id}")
    fun delete(@PathVariable("id") id: Long): ResponseEntity<Any> {
        userService!!.delete(id)
        return ResponseEntity(HttpStatus.OK)
    }

    /**
     * Assign roles to a user
     * @param id: Long
     * @param roles: List<RoleDTO>
     * @return ResponseEntity
     */
    @PostMapping("/{id}/roles")
    fun assignRoles(
            @PathVariable("id") id: Long,
            @RequestBody roles: List<RoleDTO>
    ): ResponseEntity<Any> {
        val ids: MutableList<Long> = mutableListOf()
        for (role in roles) {
            ids.add(role.id!!)
        }
        userService!!.assignRoles(id, ids)
        return ResponseEntity(HttpStatus.OK)
    }
}