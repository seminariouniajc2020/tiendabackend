package com.seminario.api.auth.dao

import com.seminario.api.auth.models.Permission
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface PermissionRepository: JpaRepository<Permission, Long>