package com.seminario.api.auth.dto

import com.fasterxml.jackson.annotation.JsonInclude
import com.seminario.api.auth.models.Compra
import com.seminario.api.auth.models.Producto
import com.seminario.api.auth.models.User
import java.util.*

@JsonInclude(JsonInclude.Include.NON_NULL)
data class CompraDTO(
        var id: Long? = null,
        var estado: String? = null,
        var cantidad: Int? = null,
        var producto: ProductoDTO? = null,
        var total: Int? = null,
        var fecha: Date? = Date(),
        var user: UserDTO? = null

) {
    constructor(compra: Compra) : this() {
        this.id = compra.id
        this.estado = compra.estado
        this.cantidad = compra.cantidad
        this.producto = ProductoDTO(compra.producto!!)
        this.total = compra.total
        this.fecha = compra.fecha
        this.user = UserDTO(compra.user!!, false)
    }
}