package com.seminario.api.auth.dto

import com.fasterxml.jackson.annotation.JsonInclude
import com.seminario.api.auth.models.Permission
import java.io.Serializable

@JsonInclude(JsonInclude.Include.NON_NULL)
data class PermissionDTO(
        var id: Long? = null,
        var name: String? = null,
        var description: String? = null,
        var roles: List<RoleDTO>? = null
): Serializable {

    constructor(permission: Permission, showRoles: Boolean): this() {
        this.id = permission.id
        this.name = permission.name
        this.description = permission.description
        if (showRoles) {
            this.roles = permission.roles?.map {
                role -> RoleDTO(role, false)
            }
        }
    }
}