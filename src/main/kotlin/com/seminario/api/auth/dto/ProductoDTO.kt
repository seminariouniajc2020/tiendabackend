package com.seminario.api.auth.dto

import com.fasterxml.jackson.annotation.JsonInclude
import com.seminario.api.auth.models.Producto

@JsonInclude(JsonInclude.Include.NON_NULL)
data class ProductoDTO(
        var id: Long? = null,
        var nombre: String? = null,
        var tipo_de_producto: String? = null,
        var cantidad: Int?= null,
        var precio:Int? = null,
        var talla: String? = null,
        var caracteristica_persona: String? = null,
        var color: String? = null,
        var calidad: String? = null,
        var imagen: String? = null,
        var id_empresa: Int? = null
    ) {
    constructor(producto: Producto) : this() {
        this.id = producto.id
        this.nombre = producto.nombre
        this.tipo_de_producto = producto.tipo_de_producto
        this.talla = producto.talla
        this.caracteristica_persona = producto.caracteristica_persona
        this.color = producto.color
        this.calidad = producto.calidad
        this.precio = producto.precio
        this.imagen = producto.imagen
    }
}