package com.seminario.api.auth.dto

data class ReqCompra(
        var idProducto: Long? = null,
        var idUser: Long? = null,
        var cantidad: Int? = null
)