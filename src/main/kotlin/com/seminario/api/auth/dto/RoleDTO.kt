package com.seminario.api.auth.dto

import com.fasterxml.jackson.annotation.JsonInclude
import com.seminario.api.auth.models.Role
import java.io.Serializable

@JsonInclude(JsonInclude.Include.NON_NULL)
data class RoleDTO(
        var id: Long? = null,
        var name: String? = null,
        var description: String? = null,
        var permissions: List<PermissionDTO>? = null,
        val users: List<UserDTO>? = null
): Serializable {

    constructor(role: Role, showPermissions: Boolean): this() {
        this.id = role.id
        this.name = role.name
        this.description = role.description
        if (showPermissions) {
            this.permissions = role.permissions?.map {
                permission -> PermissionDTO(permission, false)
            }
        }
    }
}