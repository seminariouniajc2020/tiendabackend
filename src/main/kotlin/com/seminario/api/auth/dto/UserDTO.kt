package com.seminario.api.auth.dto

import com.fasterxml.jackson.annotation.JsonInclude
import com.seminario.api.auth.models.User

@JsonInclude(JsonInclude.Include.NON_NULL)
data class UserDTO(
        var id: Long? = null,
        var identification: String? = null,
        var name: String? = null,
        var lastName: String? = null,
        var phone: String? = null,
        var picture: String? = null,
        var credits: Int? = null,
        var email: String? = null,
        var password: String? = "",
        var enable: Boolean? = null,
        var roles: List<RoleDTO>? = null
) {
    constructor(user: User, showRoles: Boolean) : this() {
        this.id = user.id
        this.identification = user.identification
        this.name = user.name
        this.lastName = user.lastName
        this.phone = user.phone
        this.credits = user.credits
        this.picture = user.picture
        this.email = user.username
        if (showRoles) {
            this.roles = user.roles?.map {
                role -> RoleDTO(role, true)
            }
        }

    }
}