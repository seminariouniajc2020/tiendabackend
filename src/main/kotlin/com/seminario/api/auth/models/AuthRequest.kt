package com.seminario.api.auth.models

import java.io.Serializable

data class AuthRequest(
        val email: String = "",
        val password: String = ""
): Serializable