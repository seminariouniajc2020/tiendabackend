package com.seminario.api.auth.models

data class AuthResponse(
        val accessToken: String? = "",
        val prefix: String? = "Bearer"
) {
}