package com.seminario.api.auth.models

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.seminario.api.auth.dto.CompraDTO
import java.util.*
import javax.persistence.*


@Entity
@Table(name = "compra")
data class Compra (
        var estado: String = "",
        var cantidad: Int? = 0,
        @ManyToOne
        @JoinColumn(name="id_producto")
        var producto: Producto? = null,
        var total: Int? = 0,
        var fecha: Date = Date(),
        @ManyToOne
        @JoinColumn(name="id_carrito_compra")
        var user: User? = null
) {
    constructor(compra: CompraDTO): this() {
        this.estado = compra.estado!!
        this.cantidad = compra.cantidad!!
        this.producto = Producto(compra.producto!!)
        this.total = compra.total!!
        this.fecha = compra.fecha!!
        this.user = User(compra.user!!)
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id:Long = 0
}