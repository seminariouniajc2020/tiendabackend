package com.seminario.api.auth.models

import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import java.util.stream.Collectors
import kotlin.collections.ArrayList

class MyUserDetails(): UserDetails {

    private var identification:String = ""
    private var name:String = ""
    private var lastName:String = ""
    private var phone:String = ""
    private var picture:String? = ""
    private var username:String = ""
    private var password:String? = ""
    private var credits: Int? = 0
    private var enable:Boolean? = false
    private var roles: List<Role>? = emptyList()
    private var permissions: ArrayList<String> = ArrayList()

    constructor(user: User) : this() {
        this.identification = user.identification
        this.name = user.name
        this.lastName = user.lastName
        this.phone = user.phone
        this.picture = user.picture
        this.credits = user.credits
        this.username = user.username
        this.password = user.password
        this.enable = user.enable
        this.roles = user.roles
        this.permissions = getPermissions(user.roles!!)
    }

    /**
     * Get a list with permissions name.
     * @param roles: List<Role>
     * @return ArrayList<String>
     */
    private fun getPermissions(roles: List<Role>): ArrayList<String> {
        val permissions: ArrayList<String> = ArrayList()
        val collection: MutableList<Permission> = ArrayList()
        roles.forEach {
            collection.addAll(it.permissions!!)
        }
        collection.forEach {
            if (!permissions.contains(it.name)) {
                permissions.add(it.name)
            }
        }
        return permissions
    }

    /**
     * Get a String with the concatenated permissions
     * @return String
     */
    fun getScope(): String {
        var scope = ""
        this.permissions.forEach {
            if (scope == "") {
                scope = it
            } else {
                scope += " $it"
            }
        }
        return scope;
    }

    override fun getAuthorities(): MutableCollection<out GrantedAuthority> {
        return this.permissions.stream().map { s -> SimpleGrantedAuthority(s) }.collect(Collectors.toList())
    }

    override fun isEnabled(): Boolean {
        return this.enable!!
    }

    override fun getUsername(): String {
        return this.username!!
    }

    override fun isCredentialsNonExpired(): Boolean {
        return true
    }

    override fun getPassword(): String {
        return this.password!!
    }

    override fun isAccountNonExpired(): Boolean {
        return true
    }

    override fun isAccountNonLocked(): Boolean {
        return true
    }
}