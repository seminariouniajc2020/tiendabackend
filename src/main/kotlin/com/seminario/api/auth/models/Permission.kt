package com.seminario.api.auth.models

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.seminario.api.auth.dto.PermissionDTO
import javax.persistence.*

@Entity
@Table( name = "permissions" )
data class Permission(
        var name: String = "",
        var description: String? = ""
) {

    constructor(permission: PermissionDTO): this() {
        this.name = permission.name!!
        this.description = permission.description
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id:Long = 0

    @ManyToMany( mappedBy = "permissions" )
    @JsonIgnoreProperties("permissions")
    var roles: List<Role>? = null
}