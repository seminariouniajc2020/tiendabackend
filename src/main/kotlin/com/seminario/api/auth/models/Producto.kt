package com.seminario.api.auth.models

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.seminario.api.auth.dto.ProductoDTO
import javax.persistence.*


@Entity
@Table(name = "producto")
data class Producto (
        var nombre: String = "",
        var tipo_de_producto: String = "",
        var cantidad: Int? = 0,
        var precio: Int? = 0,
        var talla: String = "",
        var caracteristica_persona: String = "",
        var color: String? = "",
        var calidad: String = "",
        var imagen: String = "",
        var id_empresa: Int? = 0
) {
    constructor(producto: ProductoDTO): this() {
        this.nombre = producto.nombre!!
        this.tipo_de_producto = producto.tipo_de_producto!!
        this.cantidad = producto.cantidad
        this.precio = producto.precio
        this.talla = producto.talla!!
        this.caracteristica_persona = producto.caracteristica_persona!!
        this.color = producto.color!!
        this.calidad = producto.calidad!!
        this.imagen = producto.imagen!!
        this.id_empresa = producto.id_empresa
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id:Long = 0
}