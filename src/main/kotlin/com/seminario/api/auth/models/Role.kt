package com.seminario.api.auth.models

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.seminario.api.auth.dto.RoleDTO
import javax.persistence.*

@Entity
@Table( name ="roles" )
data class Role(
        var name: String = "",
        var description: String? = null
) {

    constructor(role: RoleDTO) : this() {
        this.name = role.name!!
        this.description = role.description!!
    }

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    var id:Long = 0

    @ManyToMany( mappedBy = "roles" )
    @JsonIgnoreProperties("roles")
    var users: List<User>? = null

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "roles_permissions",
            joinColumns = [JoinColumn(name = "id_role")],
            inverseJoinColumns = [JoinColumn(name = "id_permission")]
    )
    @JsonIgnoreProperties("roles")
    var permissions: List<Permission>? = null
}