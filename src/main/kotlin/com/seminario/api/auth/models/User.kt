package com.seminario.api.auth.models

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.seminario.api.auth.dto.UserDTO
import javax.persistence.*


@Entity
@Table(name = "users")
data class User (
        var identification: String = "",
        var name: String = "",
        @Column(name = "last_name")
        var lastName: String = "",
        var phone: String = "",
        var picture: String? = "",
        @Column(name = "email")
        var username: String = "",
        @JsonIgnore
        var password: String? = "",
        var credits: Int? = 0,
        var enable: Boolean? = true
) {
    constructor(user: UserDTO): this() {
        this.identification = user.identification!!
        this.name = user.name!!
        this.lastName = user.lastName!!
        this.phone = user.phone!!
        this.picture = user.picture
        this.username = user.email!!
        this.password = user.password
        this.credits = user.credits
        this.enable = user.enable
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id:Long = 0

    @ManyToMany
    @JoinTable(
            name = "users_roles",
            joinColumns = [JoinColumn(name = "id_user")],
            inverseJoinColumns = [JoinColumn(name = "id_role")]
    )
    @JsonIgnoreProperties("users")
    var roles: List<Role>? = null
}