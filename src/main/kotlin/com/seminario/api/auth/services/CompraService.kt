package com.seminario.api.auth.services

import com.seminario.api.auth.dao.CompraRepository
import com.seminario.api.auth.dao.ProductoRepository
import com.seminario.api.auth.dao.UserRepository
import com.seminario.api.auth.models.Compra
import com.seminario.api.auth.models.Permission
import com.seminario.api.auth.models.Producto
import com.seminario.api.auth.models.User
import com.seminario.api.exceptions.AlreadyExistsException
import com.seminario.api.exceptions.BusinessException
import com.seminario.api.exceptions.NotFoundException
import com.seminario.api.utils.Constants
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*


@Service
class CompraService: ICompraServices {

    @Autowired
    val compraRepository: CompraRepository? = null

    @Autowired
    val productoRepository: ProductoRepository? = null

    @Autowired
    val userRepository: UserRepository? = null

    /**
     * Display a list of purchases
     * @throws BusinessException
     * @return List<Compra>
     */
    @Throws(BusinessException::class)
    override fun getAll(): List<Compra> {
        return try {
            compraRepository!!.findAll()
        } catch (e: Exception) {
            throw BusinessException(e.message)
        }
    }

    /**
     * Display a purchase by id
     * @param id: Long
     * @throws BusinessException
     * @return User
     */
    @Throws(BusinessException::class)
    override fun getById(id: Long): Compra {
        purchaseNotFoundException(id)
        return try {
            compraRepository!!.findById(id).get()
        } catch (e: Exception) {
            throw BusinessException(e.message)
        }
    }

    /**
     * Create a new purchase
     * @param compra: Compra
     * @throws AlreadyExistsException
     * @throws BusinessException
     */
    @Throws(BusinessException::class, AlreadyExistsException::class)
    override fun save(idProducto: Long, cantidad: Int, idUser: Long) {
        try {
            val producto = productoRepository!!.findById(idProducto)
            val user = userRepository!!.findById(idUser)
            var compra = Compra()

            compra.producto = producto.get()
            compra.cantidad = cantidad
            compra.estado = "Finalizada"
            compra.user = user.get()
            compra.total = cantidad * producto.get().precio!!

            if (user.get().credits!! < compra.total!!) {
                throw BusinessException("Créditos insuficientes")
            }

            if (!user.isPresent) {
                throw NotFoundException("No se encontró un usuario con el id: $idUser")
            }

            if (!user.isPresent) {
                throw NotFoundException("No se encontró un producto con el id: $idProducto")
            }

            user.get().credits = user.get().credits!!.minus(compra.total!!)

            compraRepository!!.save(compra)
            userRepository!!.save(user.get())
        } catch (e: Exception) {
            throw BusinessException(e.message)
        }
    }

    /**
     * Validate if the u exists
     * @param id: Long
     * @throws BusinessException
     * @return Boolean
     */
    @Throws(BusinessException::class)
    private fun purchaseExistsById(id: Long ): Boolean {
        val op: Optional<Compra>
        try {
            op = compraRepository!!.findById(id)
        } catch (e: Exception) {
            throw BusinessException(e.message)
        }
        return op.isPresent
    }

    /**
     * Throw a exception if the purchase no exists
     * @param id: Long
     * @throws NotFoundException
     */
     fun purchaseNotFoundException(id: Long) {
        if (!purchaseExistsById(id)) {
            throw NotFoundException("${Constants.MESSAGE_COMPRA_NOT_FOUND} con id: $id")
        }
    }
}