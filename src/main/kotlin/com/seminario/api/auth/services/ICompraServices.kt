package com.seminario.api.auth.services

import com.seminario.api.auth.models.Compra

interface ICompraServices {
    fun getAll(): List<Compra>
    fun getById( id: Long ): Compra
    fun save( idProducto: Long, cantidad: Int, idUser: Long )
}