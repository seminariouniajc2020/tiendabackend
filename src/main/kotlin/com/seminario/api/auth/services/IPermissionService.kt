package com.seminario.api.auth.services

import com.seminario.api.auth.models.Permission

interface IPermissionService {
    fun getAll(): List<Permission>
    fun getById( id: Long ): Permission
    fun save( permission: Permission)
    fun update( id: Long, permission: Permission )
    fun delete( id: Long )
}