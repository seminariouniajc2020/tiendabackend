package com.seminario.api.auth.services

import com.seminario.api.auth.models.Producto

interface IProductoService {
    fun getAll(): List<Producto>
    fun getById( id: Long ): Producto
}