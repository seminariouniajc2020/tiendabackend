package com.seminario.api.auth.services

import com.seminario.api.auth.models.Role

interface IRoleService {
    fun getAll(): List<Role>
    fun getById( id: Long ): Role
    fun save( role: Role )
    fun update( id: Long, role: Role )
    fun delete( id: Long )
    fun assignPermissions ( id: Long, permissions: List<Long> )
}