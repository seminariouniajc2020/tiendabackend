package com.seminario.api.auth.services

import com.seminario.api.auth.utils.JwtUtil
import com.seminario.api.auth.dao.RoleRepository
import com.seminario.api.auth.dao.UserRepository
import com.seminario.api.auth.models.MyUserDetails
import com.seminario.api.auth.models.Role
import com.seminario.api.auth.models.User
import com.seminario.api.exceptions.AlreadyExistsException
import com.seminario.api.exceptions.BusinessException
import com.seminario.api.exceptions.NotFoundException
import com.seminario.api.utils.Constants
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Service
import java.lang.Exception
import java.util.*

@Service
class MyUserDetailsService: UserDetailsService {

    @Autowired
    val userRepository: UserRepository? = null

    @Autowired
    val roleRepository: RoleRepository? = null

    @Autowired
    private val jwtUtil: JwtUtil? = null

    private val idRoleDefault: Long = 3

    /**
     * Load a user by username
     * @param username: String
     * @throws NotFoundException
     * @return MyUserDetails
     */
    @Throws(NotFoundException::class)
    override fun loadUserByUsername(username: String): MyUserDetails {
        val user: Optional<User> = userByUsername(username)
        if (!user.isPresent) {
            throw NotFoundException("${Constants.MESSAGE_USER_NOT_FOUND} con nombre de usuario: $username")
        } else {
            return MyUserDetails(user.get())
        }
    }

    /**
     * Display a list of users
     * @throws BusinessException
     * @return List<Users>
     */
    @Throws(BusinessException::class)
    fun getAll(): List<User> {
        return try {
            userRepository!!.findAll()
        } catch (e: Exception) {
            throw BusinessException(e.message)
        }
    }

    /**
     * Display a user by id
     * @param id: Long
     * @throws BusinessException
     * @return User
     */
    @Throws(BusinessException::class)
    fun getById(id: Long): User {
        userNotFoundException(id)
        return try {
            userRepository!!.findById(id).get()
        } catch (e: Exception) {
            throw BusinessException(e.message)
        }
    }

    /**
     * Create a new user
     * @param user: User
     * @throws AlreadyExistsException
     * @throws BusinessException
     */
    @Throws(BusinessException::class, AlreadyExistsException::class)
    fun save(user: User) {
        val userVerify: Optional<User> = userByUsername(user.username)
        if (userVerify.isPresent) {
            throw AlreadyExistsException("${Constants.MESSAGE_USERNAME_EXISTS}: ${user.username}")
        } else {
            try {
                val encryptPassword: String = hashPassword(user.password!!)
                //default user role
                val role: Role = roleRepository!!.findById(idRoleDefault).get()
                user.password  = encryptPassword
                user.roles = listOf(role)
                user.enable = true
                userRepository!!.save(user)
            } catch (e: Exception) {
                throw BusinessException(e.message)
            }
        }
    }

    /**
     * Update a user
     * @param id: Long
     * @throws NotFoundException
     * @throws AlreadyExistsException
     */
    @Throws(BusinessException::class, AlreadyExistsException::class)
    fun update(id: Long, user: User) {
        val newUser: User = userById(id).get()
        val op: Optional<User> = userByUsername(user.username)
        if ( op.isPresent && op.get().id != id ) {
            throw AlreadyExistsException("${Constants.MESSAGE_USERNAME_EXISTS}: ${user.username}")
        } else {
            try {
                newUser.identification = user.identification
                newUser.name = user.name
                newUser.lastName = user.lastName
                newUser.phone = user.phone
                newUser.credits = user.credits
                newUser.password = hashPassword(user.password!!)
                newUser.username = user.username
                newUser.enable = user.enable
                userRepository!!.save(newUser)
            } catch (e: Exception) {
                throw BusinessException(e.message)
            }
        }
    }

    /**
     * Delete a user
     * @param id: Long
     * @throws BusinessException
     */
    @Throws(BusinessException::class)
    fun delete(id: Long) {
        userNotFoundException(id)
        try {
            userRepository!!.deleteById(id)
        } catch (e: Exception) {
            throw BusinessException(e.message)
        }
    }

    /**
     * Assign roles to a user
     * @param id: String
     * @param roles List<Long>
     * @throws BusinessException
     */
    @Throws(BusinessException::class)
    fun assignRoles(id: Long, roles: List<Long>) {
        val user: User = userById(id).get()
        try {
            val rolesList: List<Role> = roleRepository!!.findAllById(roles)
            user.roles = rolesList
            userRepository!!.save(user)
        } catch (e: Exception) {
            throw BusinessException(e.message)
        }
    }

    /**
     * Display user information by token
     * @param token: String
     * @throws NotFoundException
     * @return User
     */
    @Throws(NotFoundException::class)
    fun userinfo(token: String): User {
        val jwt: String = token.substring(7)
        val username: String = jwtUtil!!.extractUsername(jwt)
        val user: Optional<User> = userByUsername(username)
        if ( user.isPresent ) {
            return user.get()
        } else {
            throw NotFoundException("${Constants.MESSAGE_USER_NOT_FOUND} con nombre de usuario: $username")
        }
    }

    /**
     * Update user information
     * @param token: String
     * @param user: User
     * @throws AlreadyExistsException
     * @throws BusinessException
     */
    @Throws(BusinessException::class, AlreadyExistsException::class)
    fun updateUserinfo(token: String, user: User) {
        val op: Optional<User> = userByUsername(user.username)
        val newUser: User = userinfo(token)
        if ( op.isPresent && op.get().id != newUser.id ) {
            throw AlreadyExistsException("${Constants.MESSAGE_USERNAME_EXISTS}: ${user.username}")
        } else {
            try {
                newUser.identification = user.identification
                newUser.name = user.name
                newUser.lastName = user.lastName
                newUser.phone = user.phone
                newUser.credits = user.credits
                newUser.username = user.username
                newUser.picture = user.picture
                if(user.password != null && user.password != "") {
                    newUser.password = hashPassword(user.password!!)
                }
                userRepository!!.save(newUser)
            } catch (e: Exception) {
                throw BusinessException(e.message)
            }
        }
    }

    /**
     * Get a user by id
     * @param id: Long
     * @throws NotFoundException
     * @throws BusinessException
     * @return Optional<User>
     */
    @Throws(BusinessException::class, NotFoundException::class)
    private fun userById( id: Long ): Optional<User> {
        val op: Optional<User>
        try {
            op = userRepository!!.findById(id)
        } catch (e: Exception) {
            throw BusinessException(e.message)
        }
        if(!op.isPresent){
            throw NotFoundException("${Constants.MESSAGE_USER_NOT_FOUND} con el id: $id")
        }
        return op
    }

    /**
     * Validate if the user exists
     * @param id: Long
     * @throws BusinessException
     * @return Boolean
     */
    @Throws(BusinessException::class)
    private fun userExistsById( id: Long ): Boolean {
        val op: Optional<User>
        try {
            op = userRepository!!.findById(id)
        } catch (e: Exception) {
            throw BusinessException(e.message)
        }
        return op.isPresent
    }

    /**
     * Throw a exception if the user no exists
     * @param id: Long
     * @throws NotFoundException
     */
    private fun userNotFoundException(id: Long) {
        if (!userExistsById(id)) {
            throw NotFoundException("${Constants.MESSAGE_USER_NOT_FOUND} con id: $id")
        }
    }

    /**
     * Get user by username
     * @param username: String
     * @throws BusinessException
     * @return Optional<User>
     */
    @Throws(BusinessException::class)
    private fun userByUsername( username: String ): Optional<User> {
        val op: Optional<User>
        try {
            op = userRepository!!.findByUsername(username)
        } catch (e: Exception) {
            throw BusinessException(e.message)
        }
        return op
    }

    /**
     * Encrypt the paswword
     * @param password: String
     * @return String
     */
    private fun hashPassword(password: String): String {
        val encoder = BCryptPasswordEncoder()
        return encoder.encode(password)
    }
}