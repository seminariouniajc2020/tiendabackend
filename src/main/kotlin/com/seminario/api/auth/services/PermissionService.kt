package com.seminario.api.auth.services

import com.seminario.api.auth.dao.PermissionRepository
import com.seminario.api.auth.models.Permission
import com.seminario.api.exceptions.BusinessException
import com.seminario.api.exceptions.NotFoundException
import com.seminario.api.utils.Constants
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*

@Service
class PermissionService: IPermissionService {

    @Autowired
    val permissionRepository: PermissionRepository? = null

    /**
     * Display a list of permissions
     * @throws BusinessException
     * @return List<Permission>
     */
    @Throws(BusinessException::class)
    override fun getAll(): List<Permission> {
        return try {
            permissionRepository!!.findAll()
        } catch (e: Exception) {
            throw BusinessException(e.message)
        }
    }

    /**
     * Display a permission
     * @param id: Long
     * @throws BusinessException
     * @return Permission
     */
    override fun getById(id: Long): Permission {
        permissionNotFoundException(id)
        return try {
            permissionRepository!!.findById(id).get()
        } catch (e: Exception) {
            throw BusinessException(e.message)
        }
    }

    /**
     * Create a new permission
     * @param permission: Permission
     * @throws BusinessException
     */
    @Throws(BusinessException::class)
    override fun save(permission: Permission) {
        try {
            permissionRepository!!.save(permission)
        } catch (e: Exception) {
            throw BusinessException(e.message)
        }
    }

    /**
     * Update a permission
     * @param id: Long
     * @param permission: Permission
     * @throws BusinessException
     */
    @Throws(BusinessException::class)
    override fun update(id: Long, permission: Permission) {
        permissionNotFoundException(id)
        try {
            val newPermission: Permission = permissionRepository!!.findById(id).get()
            newPermission.name = permission.name
            newPermission.description = permission.description
            permissionRepository!!.save(newPermission)
        } catch (e: BusinessException) {
            throw BusinessException(e.message)
        }
    }

    /**
     * Delete a permission
     * @param id: Long
     * @throws BusinessException
     */
    @Throws(BusinessException::class)
    override fun delete(id: Long) {
        permissionNotFoundException(id)
        try {
            permissionRepository!!.deleteById(id)
        } catch (e: Exception) {
            throw BusinessException(e.message)
        }
    }

    /**
     * Validate if the permission exists
     * @param id: Long
     * @throws BusinessException
     * @return Boolean
     */
    @Throws(BusinessException::class)
    private fun permissionExists( id: Long ): Boolean {
        val op: Optional<Permission>
        try {
            op = permissionRepository!!.findById(id)
        } catch (e: Exception) {
            throw BusinessException(e.message)
        }
        return op.isPresent
    }

    /**
     * Throw a exception if the permission no exists
     * @param id: Long
     * @throws NotFoundException
     */
    private fun permissionNotFoundException(id: Long) {
        if (!permissionExists(id)) {
            throw NotFoundException("${Constants.MESSAGE_PERMISSION_NOT_FOUND} con id: $id")
        }
    }
}