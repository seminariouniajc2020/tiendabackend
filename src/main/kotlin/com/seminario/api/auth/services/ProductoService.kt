package com.seminario.api.auth.services

import com.seminario.api.auth.dao.ProductoRepository
import com.seminario.api.auth.models.Producto
import com.seminario.api.exceptions.BusinessException
import com.seminario.api.exceptions.NotFoundException
import com.seminario.api.utils.Constants
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.lang.Exception
import java.util.*

@Service
class ProductoService: IProductoService {

    @Autowired
    val productoRepository: ProductoRepository? = null

    /**
     * Display a list of products
     * @throws BusinessException
     * @return List<Users>
     */
    @Throws(BusinessException::class)
    override fun getAll(): List<Producto> {
        return try {
            productoRepository!!.findAll()
        } catch (e: Exception) {
            throw BusinessException(e.message)
        }
    }

    /**
     * Display a product by id
     * @param id: Long
     * @throws BusinessException
     * @return Producto
     */
    @Throws(BusinessException::class)
    override fun getById(id: Long): Producto {
        productNotFoundException(id)
        return try {
            productoRepository!!.findById(id).get()
        } catch (e: Exception) {
            throw BusinessException(e.message)
        }
    }

    /**
     * Validate if the product exists
     * @param id: Long
     * @throws BusinessException
     * @return Boolean
     */
    @Throws(BusinessException::class)
    private fun productExistsById(id: Long ): Boolean {
        val op: Optional<Producto>
        try {
            op = productoRepository!!.findById(id)
        } catch (e: Exception) {
            throw BusinessException(e.message)
        }
        return op.isPresent
    }

    /**
     * Throw a exception if the product no exists
     * @param id: Long
     * @throws NotFoundException
     */
    private fun productNotFoundException(id: Long) {
        if (!productExistsById(id)) {
            throw NotFoundException("${Constants.MESSAGE_PRODUCT_NOT_FOUND} con id: $id")
        }
    }

}