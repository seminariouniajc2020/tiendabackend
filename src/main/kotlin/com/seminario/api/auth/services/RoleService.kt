package com.seminario.api.auth.services

import com.seminario.api.auth.dao.PermissionRepository
import com.seminario.api.auth.dao.RoleRepository
import com.seminario.api.auth.models.Permission
import com.seminario.api.auth.models.Role
import com.seminario.api.exceptions.BusinessException
import com.seminario.api.exceptions.NotFoundException
import com.seminario.api.utils.Constants
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*

@Service
class RoleService: IRoleService {

    @Autowired
    val roleRepository: RoleRepository? = null

    @Autowired
    val permissionRepository: PermissionRepository? = null

    /**
     * Display a list of roles
     * @throws BusinessException
     * @return List<Role>
     */
    @Throws(BusinessException::class)
    override fun getAll(): List<Role> {
        return try {
            roleRepository!!.findAll()
        } catch (e: Exception) {
            throw BusinessException(e.message)
        }
    }

    /**
     * Display a role
     * @param id: Long
     * @throws BusinessException
     * @return Role
     */
    override fun getById(id: Long): Role {
        roleNotFoundException(id)
        return try {
            roleRepository!!.findById(id).get()
        } catch (e: Exception) {
            throw BusinessException(e.message)
        }
    }

    /**
     * Create a new role
     * @param role: Role
     * @throws BusinessException
     */
    @Throws(BusinessException::class)
    override fun save(role: Role) {
        try {
            roleRepository!!.save(role)
        } catch (e: Exception) {
            throw BusinessException(e.message)
        }
    }

    /**
     * Update a role
     * @param id: Long
     * @param role: Role
     * @throws BusinessException
     */
    @Throws(BusinessException::class, NotFoundException::class)
    override fun update(id: Long, role: Role) {
        roleNotFoundException(id)
        try {
            val newRole: Role = roleRepository!!.findById(id).get()
            newRole.name = role.name
            newRole.description = role.description
            roleRepository!!.save(newRole)
        } catch (e: BusinessException) {
            throw BusinessException(e.message)
        }
    }

    /**
     * Delete a role
     * @param id: long
     * @throws BusinessException
     */
    @Throws(BusinessException::class)
    override fun delete(id: Long) {
        roleNotFoundException(id)
        try {
            roleRepository!!.deleteById(id)
        } catch (e: Exception) {
            throw BusinessException(e.message)
        }
    }

    /**
     * Assign permissions to a role
     * @param id: Long
     * @param permissions: List<Long>
     * @throws BusinessException
     */
    @Throws(BusinessException::class, NotFoundException::class)
    override fun assignPermissions(id: Long, permissions: List<Long>) {
        roleNotFoundException(id)
        try {
            val role: Role = roleRepository!!.findById(id).get()
            val permissionList: List<Permission> = permissionRepository!!.findAllById(permissions)
            role.permissions = permissionList
            roleRepository!!.save(role)
        } catch (e: Exception) {
            throw BusinessException(e.message)
        }
    }

    /**
     * Validate if the role exists
     * @param id: Long
     * @throws BusinessException
     * @return Boolean
     */
    @Throws(BusinessException::class)
    private fun roleExists( id: Long ): Boolean {
        val op: Optional<Role>
        try {
            op = roleRepository!!.findById(id)
        } catch (e: Exception) {
            throw BusinessException(e.message)
        }
        return op.isPresent
    }

    /**
     * Throw a exception if the role no exists
     * @param id: Long
     * @throws NotFoundException
     */
    private fun roleNotFoundException(id: Long) {
        if (!roleExists(id)) {
            throw NotFoundException("${Constants.MESSAGE_ROLE_NOT_FOUND} con id: $id")
        }
    }
}