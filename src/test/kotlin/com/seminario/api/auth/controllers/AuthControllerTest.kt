package com.seminario.api.auth.controllers

import com.fasterxml.jackson.core.JsonParseException
import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.JsonMappingException
import com.fasterxml.jackson.databind.ObjectMapper
import com.seminario.api.auth.dto.UserDTO
import com.seminario.api.auth.models.AuthRequest
import com.seminario.api.auth.models.AuthResponse
import com.seminario.api.utils.Constants
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.ResultActions
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.web.context.WebApplicationContext
import java.io.IOException

@SpringBootTest
internal class AuthControllerTest {

    @Autowired
    private var mvc: MockMvc? = null

    @Autowired
    val webApplicationContext: WebApplicationContext? = null

    @BeforeEach
    fun setUp() {
        mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext!!).build()
    }

    @Test
    fun authenticate() {
        val validCredentials= AuthRequest("admin@test.com", "admin123")
        val invalidCredentials = AuthRequest("invalid@invalid.com", "invalid")

        authenticateReq(validCredentials).andExpect(
                MockMvcResultMatchers.status().isOk
        )

        authenticateReq(invalidCredentials).andExpect(
                MockMvcResultMatchers.status().isBadRequest
        )
    }

    @Test
    fun signupSuccessful() {
        val user = UserDTO(
                identification = "123",
                name = "Test",
                lastName =  "Test",
                phone = "123",
                credits = 0,
                email = "test@test.com",
                password = "test123"
        )
        mvc!!.perform(
             MockMvcRequestBuilders
                     .post("${Constants.URL_BASE_AUTH}/signup")
                     .contentType(MediaType.APPLICATION_JSON)
                     .content(mapToJson(user)!!)
        ).andExpect(
                MockMvcResultMatchers.status().isCreated
        )
    }

    @Test
    fun userinfo() {
        mvc!!.perform(
                MockMvcRequestBuilders
                        .get("${Constants.URL_BASE_AUTH}/userinfo")
                        .header("Authorization", "Bearer ${getAccessToken()}")
        ).andExpect(
                MockMvcResultMatchers.status().isOk
        )
    }

    @Test
    fun updateUserinfo() {
        val user = UserDTO(
                identification = "1144205269",
                name = "Admin",
                lastName =  "Admin",
                phone = "123",
                credits = 2000000,
                email = "admin@test.com",
                password = "admin123"
        )
        mvc!!.perform(
                MockMvcRequestBuilders
                        .put("${Constants.URL_BASE_AUTH}/userinfo")
                        .header("Authorization", "Bearer ${getAccessToken()}")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapToJson(user)!!)
        ).andExpect(
                MockMvcResultMatchers.status().isOk
        )
    }

    @Throws(JsonProcessingException::class)
    private fun mapToJson(obj: Any?): String? {
        val objectMapper = ObjectMapper()
        return objectMapper.writeValueAsString(obj)
    }

    @Throws(JsonParseException::class, JsonMappingException::class, IOException::class)
    private fun <T> mapFromJson(json: String?, clazz: Class<T>?): T {
        val objectMapper = ObjectMapper()
        return objectMapper.readValue(json, clazz)
    }

    private fun authenticateReq(credentials: AuthRequest): ResultActions {
        return mvc!!.perform(
                MockMvcRequestBuilders
                        .post("${Constants.URL_BASE_AUTH}/token")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapToJson(credentials)!!)
        )
    }

    private fun getAccessToken(): String {
        val validCredentials = AuthRequest("admin@test.com", "admin123")
        val response: String = authenticateReq(validCredentials)
                .andReturn()
                .response.contentAsString
        val authResponse: AuthResponse = mapFromJson(response, AuthResponse::class.java)

        return authResponse.accessToken!!
    }
}