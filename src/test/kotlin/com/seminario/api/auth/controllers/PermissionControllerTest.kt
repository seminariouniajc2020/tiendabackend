package com.seminario.api.auth.controllers

import com.fasterxml.jackson.core.JsonParseException
import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.JsonMappingException
import com.fasterxml.jackson.databind.ObjectMapper
import com.seminario.api.auth.dto.PermissionDTO
import com.seminario.api.auth.models.AuthRequest
import com.seminario.api.auth.models.AuthResponse
import com.seminario.api.auth.models.Permission
import com.seminario.api.utils.Constants
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.ResultActions
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.web.context.WebApplicationContext
import java.io.IOException

@SpringBootTest
internal class PermissionControllerTest {

    @Autowired
    private var mvc: MockMvc? = null

    @Autowired
    val webApplicationContext: WebApplicationContext? = null

    @BeforeEach
    fun setUp() {
        mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext!!).build()
    }

    @Test
    fun getAll() {
        mvc!!.perform(
                MockMvcRequestBuilders
                        .get(Constants.URL_BASE_PERMISSION)
                        .header("Authorization", "Bearer ${getAccessToken()}")
        ).andExpect(
                MockMvcResultMatchers.status().isOk
        )
    }

    @Test
    fun getById() {
        val id: Long = 1
        mvc!!.perform(
                MockMvcRequestBuilders
                        .get("${Constants.URL_BASE_PERMISSION}/$id")
                        .header("Authorization", "Bearer ${getAccessToken()}")
        ).andExpect(
                MockMvcResultMatchers.status().isOk
        )
    }

    @Test
    fun mutationTest() {
        val permission = PermissionDTO(
                name = "read:test",
                description = "Permission test"
        )
        mvc!!.perform(
                MockMvcRequestBuilders
                        .post(Constants.URL_BASE_PERMISSION)
                        .header("Authorization", "Bearer ${getAccessToken()}")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapToJson(Permission(permission))!!)
        ).andExpect(
                MockMvcResultMatchers.status().isCreated
        )

        mvc!!.perform(
                MockMvcRequestBuilders
                        .put("${Constants.URL_BASE_PERMISSION}/13")
                        .header("Authorization", "Bearer ${getAccessToken()}")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapToJson(Permission(permission))!!)
        ).andExpect(
                MockMvcResultMatchers.status().isOk
        )

        mvc!!.perform(
                MockMvcRequestBuilders
                        .delete("${Constants.URL_BASE_PERMISSION}/13")
                        .header("Authorization", "Bearer ${getAccessToken()}")
                        .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(
                MockMvcResultMatchers.status().isOk
        )
    }

    @Throws(JsonProcessingException::class)
    private fun mapToJson(obj: Any?): String? {
        val objectMapper = ObjectMapper()
        return objectMapper.writeValueAsString(obj)
    }

    @Throws(JsonParseException::class, JsonMappingException::class, IOException::class)
    private fun <T> mapFromJson(json: String?, clazz: Class<T>?): T {
        val objectMapper = ObjectMapper()
        return objectMapper.readValue(json, clazz)
    }

    private fun authenticateReq(credentials: AuthRequest): ResultActions {
        return mvc!!.perform(
                MockMvcRequestBuilders
                        .post("${Constants.URL_BASE_AUTH}/token")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapToJson(credentials)!!)
        )
    }

    private fun getAccessToken(): String {
        val validCredentials = AuthRequest("admin@test.com", "admin123")
        val response: String = authenticateReq(validCredentials)
                .andReturn()
                .response.contentAsString
        val authResponse: AuthResponse = mapFromJson(response, AuthResponse::class.java)

        return authResponse.accessToken!!
    }
}