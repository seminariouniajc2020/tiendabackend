package com.seminario.api.auth.controllers

import com.fasterxml.jackson.core.JsonParseException
import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.JsonMappingException
import com.fasterxml.jackson.databind.ObjectMapper
import com.seminario.api.auth.dto.RoleDTO
import com.seminario.api.auth.dto.UserDTO
import com.seminario.api.auth.models.AuthRequest
import com.seminario.api.auth.models.AuthResponse
import com.seminario.api.auth.models.User
import com.seminario.api.utils.Constants
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.ResultActions
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.web.context.WebApplicationContext
import java.io.IOException

@SpringBootTest
internal class UserControllerTest {

    @Autowired
    private var mvc: MockMvc? = null

    @Autowired
    val webApplicationContext: WebApplicationContext? = null

    @BeforeEach
    fun setUp() {
        mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext!!).build()
    }

    @Test
    fun getAll() {
        mvc!!.perform(
                MockMvcRequestBuilders
                        .get(Constants.URL_BASE_USERS)
                        .header("Authorization", "Bearer ${getAccessToken()}")
        ).andExpect(
                MockMvcResultMatchers.status().isOk
        )
    }

    @Test
    fun getById() {
        val id: Long = 1
        mvc!!.perform(
                MockMvcRequestBuilders
                        .get("${Constants.URL_BASE_USERS}/$id")
                        .header("Authorization", "Bearer ${getAccessToken()}")
        ).andExpect(
                MockMvcResultMatchers.status().isOk
        )
    }

    @Test
    fun mutationTest() {
        val user = UserDTO(
                identification = "123",
                name = "Test2",
                lastName =  "Test2",
                phone = "123",
                credits = 0,
                enable = false,
                picture = "picture",
                email = "tests2@test.com",
                password = "test123"
        )
        val user2 = UserDTO(
                identification = "123",
                name = "Test2",
                lastName =  "Test2",
                phone = "123",
                credits = 0,
                enable = false,
                picture = "picture",
                email = "test2@test2.co",
                password = "test123"
        )
        mvc!!.perform(
                MockMvcRequestBuilders
                        .post(Constants.URL_BASE_USERS)
                        .header("Authorization", "Bearer ${getAccessToken()}")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapToJson(user)!!)
        ).andExpect(
                MockMvcResultMatchers.status().isCreated
        )
        mvc!!.perform(
                MockMvcRequestBuilders
                        .put("${Constants.URL_BASE_USERS}/4")
                        .header("Authorization", "Bearer ${getAccessToken()}")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapToJson(user2)!!)
        ).andExpect(
                MockMvcResultMatchers.status().isOk
        )
        mvc!!.perform(
                MockMvcRequestBuilders
                        .delete("${Constants.URL_BASE_USERS}/5")
                        .header("Authorization", "Bearer ${getAccessToken()}")
                        .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(
                MockMvcResultMatchers.status().isOk
        )
    }

    @Test
    fun assignRoles() {
        val id: Long = 2
        val roles: List<RoleDTO> = listOf(
                RoleDTO(
                        id = 1
                )
        )
        mvc!!.perform(
                MockMvcRequestBuilders
                        .post("${Constants.URL_BASE_USERS}/$id/roles")
                        .header("Authorization", "Bearer ${getAccessToken()}")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapToJson(roles)!!)
        ).andExpect(
                MockMvcResultMatchers.status().isOk
        )
    }

    @Throws(JsonProcessingException::class)
    private fun mapToJson(obj: Any?): String? {
        val objectMapper = ObjectMapper()
        return objectMapper.writeValueAsString(obj)
    }

    @Throws(JsonParseException::class, JsonMappingException::class, IOException::class)
    private fun <T> mapFromJson(json: String?, clazz: Class<T>?): T {
        val objectMapper = ObjectMapper()
        return objectMapper.readValue(json, clazz)
    }

    private fun authenticateReq(credentials: AuthRequest): ResultActions {
        return mvc!!.perform(
                MockMvcRequestBuilders
                        .post("${Constants.URL_BASE_AUTH}/token")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapToJson(credentials)!!)
        )
    }

    private fun getAccessToken(): String {
        val validCredentials = AuthRequest("admin@test.com", "admin123")
        val response: String = authenticateReq(validCredentials)
                .andReturn()
                .response.contentAsString
        val authResponse: AuthResponse = mapFromJson(response, AuthResponse::class.java)

        return authResponse.accessToken!!
    }
}