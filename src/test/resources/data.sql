insert into permissions (id, name, description) values(1,'read:permissions', 'Read permissions');
insert into permissions (id, name, description) values(2,'create:permissions', 'Create permissions');
insert into permissions (id, name, description) values(3,'update:permissions', 'Update permissions');
insert into permissions (id, name, description) values(4,'delete:permissions', 'Delete permissions');
insert into permissions (id, name, description) values(5,'read:roles', 'Read roles');
insert into permissions (id, name, description) values(6,'create:roles', 'Create roles');
insert into permissions (id, name, description) values(7,'update:roles', 'Update roles');
insert into permissions (id, name, description) values(8,'delete:roles', 'Delete roles');
insert into permissions (id, name, description) values(9,'read:users', 'Read users');
insert into permissions (id, name, description) values(10,'create:users', 'Create users');
insert into permissions (id, name, description) values(11,'update:users', 'Update users');
insert into permissions (id, name, description) values(12,'delete:users', 'Delete users');

insert into roles (id, name, description) values(1,'Administrator', 'Administrator role');
insert into roles (id, name, description) values(2,'Entrepreneur', 'Entrepreneur role');
insert into roles (id, name, description) values(3,'User', 'User role');

insert into users (id, identification, name, last_name, phone, picture, credits, email, password, enable)
values(1, '123', 'Admin', 'Admin', '123', 'picture', 1000000, 'admin@test.com', '$2a$10$4ue7RWHuCCEetR8u4sHNtOWgGyE/3EnI9YKugtbqHtKaF0jrYtZkO', true);
insert into users (id, identification, name, last_name, phone, picture, credits, email, password, enable)
values(2, '123', 'Entrepreneur', 'Entrepreneur', '123', 'picture', 100000, 'entrepreneur@test.com', '$2a$10$4ue7RWHuCCEetR8u4sHNtOWgGyE/3EnI9YKugtbqHtKaF0jrYtZkO', true);
insert into users (id, identification, name, last_name, phone, picture, credits, email, password, enable)
values(3, '123', 'User', 'User', '123', 'picture', 1000000, 'user@test.com', '$2a$10$4ue7RWHuCCEetR8u4sHNtOWgGyE/3EnI9YKugtbqHtKaF0jrYtZkO', true);
insert into users (id, identification, name, last_name, phone, picture, credits, email, password, enable)
values(4, '123', 'Disabled', 'Disabled', '123', 'picture', 1000000, 'disabled@test.com', '$2a$10$4ue7RWHuCCEetR8u4sHNtOWgGyE/3EnI9YKugtbqHtKaF0jrYtZkO', false);

insert into roles_permissions (id_role, id_permission) values (1,1);
insert into roles_permissions (id_role, id_permission) values (1,2);
insert into roles_permissions (id_role, id_permission) values (1,3);
insert into roles_permissions (id_role, id_permission) values (1,4);
insert into roles_permissions (id_role, id_permission) values (1,5);
insert into roles_permissions (id_role, id_permission) values (1,6);
insert into roles_permissions (id_role, id_permission) values (1,7);
insert into roles_permissions (id_role, id_permission) values (1,8);
insert into roles_permissions (id_role, id_permission) values (1,9);
insert into roles_permissions (id_role, id_permission) values (1,10);
insert into roles_permissions (id_role, id_permission) values (1,11);
insert into roles_permissions (id_role, id_permission) values (1,12);

insert into users_roles (id_user, id_role) values(1,1);